import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user = {
    name: '',
    lastName: '',
    email: ''
  };
  submitted = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  update(): void {
    const data = {
      name: this.user.name,
      lastName: this.user.lastName,
      email: this.user.email
    };

    this.userService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  create(): void {
    this.submitted = false;
    this.user = {
      name: '',
      lastName: '',
      email: ''
    };
  }


}

