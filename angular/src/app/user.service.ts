import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './user';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  private baseUrl =  'http://localhost:8080/api';
  private usersUrl = `${this.baseUrl}/users/`;
  private userUrl = `${this.baseUrl}/user/`;

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(`${this.usersUrl}`);
  }
  get(id): Observable<User>{
    return this.http.get<User>(`${this.userUrl}/${id}`);
  }
  create(data): Observable<any> {
    return this.http.post(this.userUrl, data);
  }
  update(id, data): Observable<any> {
    return this.http.put(`${this.userUrl}/${id}`, data);
  }
  delete(id): Observable<User>{
    return this.http.delete<User>(`${this.userUrl}/${id}`);
  }

}
