package com.app.spring.controller;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

import org.springframework.http.HttpStatus;
import unit.test.api.ObjectInit;
import java.util.List;

import com.app.spring.model.User;
import org.springframework.http.ResponseEntity;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void GetUsers() {

        UserController userController = ObjectInit.random(UserController.class);
        List<User> invokeResult = userController.getUsers();

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }


    @Test
    public void GetUserById() {
        // Initialize the object to be tested
        UserController userController = ObjectInit.random(UserController.class);
        // Initialize params of the method
        long id = ObjectInit.random(long.class);
        ResponseEntity<User> invokeResult = userController.getUserById(id);

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }


    @Test
    public void CreateUser() {
        // Initialize the object to be tested
        UserController userController = ObjectInit.random(UserController.class);
        // Initialize params of the method
        User user = ObjectInit.random(User.class);
        ResponseEntity<User> invokeResult = userController.createUser(user);

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }


    @Test
    public void UpdateUser() {
        // Initialize the object to be tested
        UserController userController = ObjectInit.random(UserController.class);
        // Initialize params of the method
        long id = ObjectInit.random(long.class);
        User user = ObjectInit.random(User.class);
        ResponseEntity<User> invokeResult = userController.updateUser(id, user);

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }


    @Test
    public void DeleteUser() {
        // Initialize the object to be tested
        UserController userController = ObjectInit.random(UserController.class);
        // Initialize params of the method
        long id = ObjectInit.random(long.class);
        ResponseEntity<HttpStatus> invokeResult = userController.deleteUser(id);

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }
}
