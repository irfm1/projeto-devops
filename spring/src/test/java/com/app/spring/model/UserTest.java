package com.app.spring.model;
import org.junit.Test;
import javax.persistence.*;
import static org.junit.Assert.*;

import org.junit.Assert;
import unit.test.api.ObjectInit;
 
public class UserTest {

 
    @Test
    public void testGetId() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        Long invokeResult = user.getId();

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }

 
    @Test
    public void testSetId() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        // Initialize params of the method
        long id = ObjectInit.random(long.class);
        user.setId(id);

        // Write the Assert code
        Assert.assertTrue(true);
    }

 
    @Test
    public void testGetName() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        String invokeResult = user.getName();

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }

 
    @Test
    public void testSetName() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        // Initialize params of the method
        String name = ObjectInit.random(String.class);
        user.setName(name);

        // Write the Assert code
        Assert.assertTrue(true);
        //Assert.assertEquals(expected, actual);
    }

 
    @Test
    public void testGetlastName() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        String invokeResult = user.getlastName();

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }

 
    @Test
    public void testSetlastName() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        // Initialize params of the method
        String lastName = ObjectInit.random(String.class);
        user.setlastName(lastName);

        // Write the Assert code
        Assert.assertTrue(true);
        //Assert.assertEquals(expected, actual);
    }

 
    @Test
    public void testGetEmail() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        String invokeResult = user.getEmail();

        // Write the Assert code
        //Assert.assertEquals(expected, invokeResult);
    }

 
    @Test
    public void testSetEmail() { 
        // Initialize the object to be tested
        User user = ObjectInit.random(User.class);
        // Initialize params of the method
        String email = ObjectInit.random(String.class);
        user.setEmail(email);

        // Write the Assert code
        Assert.assertTrue(true);
        //Assert.assertEquals(expected, actual);
    }
}