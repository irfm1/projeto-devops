package com.app.spring;

import com.app.spring.model.User;
import com.app.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
//        this.userRepository.save(new User("Teste","Bruto","teste_bt@testera.com"));
//        this.userRepository.save(new User("Peste","Puto","peste_pt@testera.com"));
//        this.userRepository.save(new User("Leste","Luto","leste_lt@testera.com"));
    }
}

