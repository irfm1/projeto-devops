package com.app.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MyEnvironmentPostProcessor implements EnvironmentPostProcessor {


    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment,
                                       SpringApplication application) {
        Resource resource = new FileSystemResource("/run/secrets/db_password");
        if (resource.exists()) {
            try{
//                if (log.isInfoEnabled()){
//                    log.info("Using database password from docker secret file");
//                }
                String dbPassword = StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset()).trim();
                Properties props = new Properties();

                props.put("spring.datasource.password", dbPassword);
                environment.getPropertySources().addFirst(new PropertiesPropertySource("dbProps", props));
            } catch (IOException e) {
                throw new RuntimeException();
            }
        } else {
            System.out.println("Conteudo nao encontrado");
        }

    }

}
